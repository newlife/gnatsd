FROM golang:1.11

MAINTAINER Ivan Kozlovic <ivan@synadia.com>

COPY . /go/src/gitee.com/newlife/gnatsd
WORKDIR /go/src/gitee.com/newlife/gnatsd

RUN CGO_ENABLED=0 go install -v -a -tags netgo -installsuffix netgo -ldflags "-s -w -X gitee.com/newlife/gnatsd/server.gitCommit=`git rev-parse --short HEAD`"

EXPOSE 4222 8222
ENTRYPOINT ["gnatsd"]
CMD ["--help"]
